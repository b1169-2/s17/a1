let students = []
console.log(students)


//3
function addStudent(newStudent){
    students.push(newStudent);
    return `${newStudent} was aded to the student's list.`
}

console.log(addStudent("John"));
console.log(addStudent("Jane"));
console.log(addStudent("Joe"));

//4
function countStudents(){
    return `There are total of ${students.length} students enrolled.`
}

console.log(countStudents())

//5
function printStudents(){
    students.forEach(
        function(element){
            console.log(element)
        }
    )
}

printStudents()

//6

function findStudent(studentName){
    if (students.includes(studentName)){
        return `${studentName} is an enrollee.`
    } else return `${studentName} is not an enrollee`
}
    
console.log(findStudent(`Joe`))
console.log(findStudent(`bill`))

